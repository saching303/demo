﻿namespace DocUpload.Core.Model
{
    public class Recipient
    {
        public virtual int Id
        {
            get;
            protected set;
        }

        public virtual string Name
        {
            get;
            set;
        }
    }
}
