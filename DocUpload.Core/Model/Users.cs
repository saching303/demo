﻿
namespace DocUpload.Core.Model
{
    public class User
    {
        public virtual UserRole UserRole
        {
            get;
            set;
        }

        public virtual UserProfile UserDetail
        {
            get;
            set;
        }

        public int ServiceAccountId
        {
            get;
            set;
        }
    }
}
