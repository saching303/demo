﻿namespace DocUpload.Core.Model
{
    public class UserServiceAccount
    {
        public virtual int ServiceAccountId { get; set; }

        public virtual int Id
        {
            get;
            protected set;
        }

        public virtual int UserId
        {
            get;
            set;
        }

        public virtual ServiceAccount ServiceAccounts
        {
            get;
            set;
        }

        public virtual int RoleId
        {
            get;
            set;
        }
    }
}
