﻿using System;

namespace DocUpload.Core.Model
{
    public class UserProfile
    {
        public virtual int Id
        {
            get;
            protected set;
        }

        public virtual string FirstName
        {
            get;
            set;
        }
               
        public virtual string LastName
        {
            get;
            set;
        }
               
        public virtual string Email
        {
            get;
            set;
        }
               
        public virtual string UserName
        {
            get;
            set;
        }
               
        public virtual string Password
        {
            get;
            set;
        }
               
        public virtual bool Active
        {
            get;
            set;
        }
               
        public virtual int CreatedBy
        {
            get;
            set;
        }
               
        public virtual DateTime CreatedOn
        {
            get;
            set;
        }
               
        public virtual int UpdatedBy
        {
            get;
            set;
        }
               
        public virtual DateTime UpdatedOn
        {
            get;
            set;
        }
               
        public virtual bool PasswordChangeRequest
        {
            get;
            set;
        }
               
        public virtual int UserType
        {
            get;
            set;
        }
    }
}   