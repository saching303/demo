﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DocUpload.Core.Model
{
    public class ServiceAccount
    {
        [DisplayName("Acccount Id")]
        public virtual int Id { get; protected set; }

        [DisplayName("Acccount Name")]
        [Required(ErrorMessage="Account Name is required!")]
        public virtual string Name { get; set; }


        public virtual IEnumerable<ServiceAccountRecipient> Recipients
        {
            get;
            set;
        }

        [DisplayName("Acccount Email")]
        public virtual string Email
        {
            get;
            set;
        }

        public virtual char Active
        {
            get;
            set;
        }

         [DisplayName("Active")]
        public virtual bool IsActive {
            get
            {
                return Active.Equals('Y');
            }
            set
            {
                Active = value
                             ? 'Y'
                             : 'N';
            }
        }

         public virtual DateTime CreatedDate
         {
             get;
             set;
         }

        public virtual DateTime UpdatedDate
        {
            get;
            set;
        }

        public virtual int CreatedBy
        {
            get;
            set;
        }

        public virtual int UpdatedBy
        {
            get;
            set;
        }

        public virtual string ConnString
        {
            get;
            set;
        }

        public virtual int ParentServiceAccountId
        {
            get;
            set;
        }
    }
}
