﻿namespace DocUpload.Core.Model
{
    public class ServiceAccountRecipient
    {
        public virtual Recipient Recipient
        {
            get;
            set;
        }

        public virtual string Password
        {
            get;
            set;
        }

        public virtual string UserName
        {
            get;
            set;
        }
        
        public virtual int Id
        {
            get;
            protected set;
        }

        public virtual ServiceAccount ServiceAccount { get; set; }

        public virtual int RecipientId { get; set; }

        public virtual int ServiceAccountId { get; set; }

    }
}
