﻿using System.Collections.Generic;
using DocUpload.Core.Model;

namespace DocUpload.Core.Interface
{
    public interface IServiceAccountUserRepository
    {
        IEnumerable<UserServiceAccount> GetMappedServiceAccoutsByUser(User user);

        IEnumerable<UserProfile> GeServiceAccoutUsersByOwner(User user);

        UserProfile GetServiceAccountUserById(int userId);

        IEnumerable<UserServiceAccount> GetServiceAccountUsersByAccountIds(int[] accountIds);

        void AddNewServiceAccountUser(UserProfile user,IEnumerable<UserServiceAccount> userServiceAccount);

        void UpdateServiceAccountUser(UserProfile user, IEnumerable<UserServiceAccount> userServiceAccount);

        void DeleteServiceAccountUserById(int userId);

        UserType GetUserType(int userType);
    }
}
