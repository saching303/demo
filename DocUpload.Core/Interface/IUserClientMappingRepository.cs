﻿using System.Collections.Generic;
using DocUpload.Core.Model;

namespace DocUpload.Core.Interface
{
    public interface IUserClientMappingRepository
    {
        IEnumerable<UserClientMapping> GetMappedClientsByServiceAccount(int serviceAccountID);

        IEnumerable<Client> GetMasterClientsListByRecipient(int recipientId);

        IEnumerable<UserClientMapping> GetSelectedClient(int clientId);

        void AddUserClientMapping(UserClientMapping userclientMapping);

        void EditUserClientMapping(UserClientMapping userclientMapping);

        void DeleteUserClientMapping(UserClientMapping userclientMapping);

        void SaveUserClientMapping(IEnumerable<UserClientMapping> userclientMapping);

        void DeleteUserClientMappingById(int clientId);
    }
}
