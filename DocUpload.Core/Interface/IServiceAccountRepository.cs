﻿using System.Collections.Generic;
using DocUpload.Core.Model;

namespace DocUpload.Core.Interface
{
    public interface IServiceAccountRepository
    {
        IEnumerable<ServiceAccount> GetServiceAccountsByUser(int userId, UserProfileType userProfileType);

        ServiceAccount GetServiceAccountById(int serviceAccountId );

        void UpdateServiceAccount(ServiceAccount serviceAccount);

        void AddNewServiceAccount(ServiceAccount serviceAccount);

        void DeleteServiceAccountById(int serviceAccountId);
    }
}
