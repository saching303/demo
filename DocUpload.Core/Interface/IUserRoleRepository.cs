﻿using System.Collections.Generic;
using DocUpload.Core.Model;

namespace DocUpload.Core.Interface
{
    public interface IUserRoleRepository
    {
        IEnumerable<UserRole> GetUserRoles();
        IEnumerable<UserRoleRights> GetUserRoleRights(int roleId);
        IEnumerable<ModuleList> GetModules();
    }
}
