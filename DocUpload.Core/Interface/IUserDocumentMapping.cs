﻿using System.Collections.Generic;
using DocUpload.Core.Model;

namespace DocUpload.Core.Interface
{
    public interface IUserDocumentMapping
    {
        IEnumerable<UserDocumentMapping> GetMappedDocumentsByServiceAccount(int serviceAccountID);

        IEnumerable<Document> GetMasterDocumentsListByRecipient(int recipientId);

        IEnumerable<UserDocumentMapping> GetSelectedDocument(int documentId);

        void AddUserDocumentMapping(UserDocumentMapping userDocumentMapping);

        void EditUserDocumentMapping(UserDocumentMapping userDocumentMapping);

        void DeleteUserDocumentMapping(UserDocumentMapping userDocumentMapping);

        void SaveUserDocumentMapping(IEnumerable<UserDocumentMapping> userDocumentMapping);

        void DeleteUserDocumentMappingById(int clientId);
    }
}
