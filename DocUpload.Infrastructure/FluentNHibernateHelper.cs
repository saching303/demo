﻿using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace DocUpload.Infrastructure
{
    public static class FluentNHibernateHelper
    {
        public static string ConnectionString
        {
            get
            {
                return "Data Source=NDTS.WORLD;User Id=NDTSAPPS;Password=NDTSAPPS;Min Pool Size=0";
            }
        }

        public static ISession CreateNewSession()
        {  
                var cfg = OracleClientConfiguration.Oracle10
                    .ConnectionString(c =>
                        c.Is(ConnectionString));

                return Fluently.Configure()
                        .Database(cfg)
                        .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
                .BuildSessionFactory().OpenSession();

        }
    }
    
}
