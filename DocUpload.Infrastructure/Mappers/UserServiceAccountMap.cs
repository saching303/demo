﻿using DocUpload.Core.Model;
using FluentNHibernate.Mapping;

namespace DocUpload.Infrastructure.Mappers
{
    public class UserServiceAccountMap : ClassMap<UserServiceAccount>
    {
        public UserServiceAccountMap()
        {
            Table("UserServiceAccount");
            Id(x => x.Id).GeneratedBy.Increment();
            Map(x => x.UserId);
            Map(x => x.ServiceAccountId);
            References(x => x.ServiceAccounts).Column("ServiceAccountId");
        }
    }
}
