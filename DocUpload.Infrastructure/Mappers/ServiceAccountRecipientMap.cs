﻿
using DocUpload.Core.Model;
using FluentNHibernate.Mapping;

namespace DocUpload.Infrastructure.Mappers
{
    public class ServiceAccountRecipientMap : ClassMap<ServiceAccountRecipient>
    {
        public ServiceAccountRecipientMap()
        {
            Table("RecipientSetting");
            Id(x => x.Id).GeneratedBy.SequenceIdentity("RECIPIENTSETTING_SEQ");
            Map(x => x.UserName);
            Map(x => x.Password);
            Map(x => x.RecipientId).Column("RECEIPIENTID");
            Map(x=> x.ServiceAccountId).Column("ServiceAccountId").ReadOnly();
            References(x => x.ServiceAccount).Column("ServiceAccountId");
            References(x => x.Recipient).Column("RECEIPIENTID").ReadOnly();
        }
    }
}
