﻿using DocUpload.Core.Model;
using FluentNHibernate.Mapping;

namespace DocUpload.Infrastructure.Mappers
{
    public class RecipientMap : ClassMap<Recipient>
    {
        public RecipientMap()
        {
            Table("Recipient");
            Id(x => x.Id);
            Map(x => x.Name);
        }
    }
}
