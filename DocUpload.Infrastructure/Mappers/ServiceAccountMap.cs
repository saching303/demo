﻿using DocUpload.Core.Model;
using FluentNHibernate.Mapping;

namespace DocUpload.Infrastructure.Mappers
{
    public class ServiceAccountMap : ClassMap<ServiceAccount>
    {
        public ServiceAccountMap()
        {
            Table("ServiceAccount");
            Id(x => x.Id).GeneratedBy.SequenceIdentity("SERVICEACCOUNT_SEQ");
            Map(x => x.Name);
            Map(x => x.Email);
            Map(x => x.Active);
            Map(x => x.ConnString);
            Map(x => x.ParentServiceAccountId);
            Map(x => x.CreatedBy);
            Map(x => x.CreatedDate);
            Map(x => x.UpdatedBy);
            Map(x => x.UpdatedDate);
            HasMany(x => x.Recipients).KeyColumn("ServiceAccountId").Inverse().Cascade.All();
        }
    }
}
