﻿using DocUpload.Core.Model;
using FluentNHibernate.Mapping;

namespace DocUpload.Infrastructure.Mappers
{
    public class UserProfileMap : ClassMap<UserProfile>
    {
        public UserProfileMap()
        {
            Table("UserProfile");
            Id(x => x.Id);
            Map(x => x.FirstName);
            Map(x => x.LastName);
            Map(x => x.UserName);
            Map(x => x.Password);
            //Map(x => x.Active);
            Map(x => x.Email);
            Map(x => x.UserType);
            Map(x => x.CreatedBy);
            Map(x => x.CreatedOn);
            Map(x => x.UpdatedBy);
            Map(x => x.UpdatedOn);
            //Map(x => x.PasswordChangeRequest);
        }
    }
}
