﻿
using Autofac;
using DocUpload.Core.Interface;
using DocUpload.Infrastructure.Repository;
using NHibernate;
namespace DocUpload.Infrastructure
{
    public class DocUploadTypesModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.Register(
            //    context => FluentNHibernateHelper.CreateNewSession())
            //    .As<ISession>().InstancePerRequest();
            builder.RegisterType<ServiceAccountRepository>().As<IServiceAccountRepository>();
            builder.RegisterType<ServiceAccountUserRepository>().As<IServiceAccountUserRepository>();
            base.Load(builder);
        }
    }
}
