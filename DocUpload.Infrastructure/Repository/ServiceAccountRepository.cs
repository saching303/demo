﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocUpload.Core.Interface;
using DocUpload.Core.Model;
using NHibernate;

namespace DocUpload.Infrastructure.Repository
{
    public class ServiceAccountRepository : IServiceAccountRepository
    {
        readonly ISession _session;

        public ServiceAccountRepository(ISession session)
        {
            _session = session;
        }

        public IEnumerable<ServiceAccount> GetServiceAccountsByUser(int userId, UserProfileType userProfileType)
        {
            //return _session.QueryOver<UserServiceAccount>().List().Where(t => t.UserId.Equals(2));
            IEnumerable<ServiceAccount> serviceAccounts = new List<ServiceAccount>();
           
            // based on the user type get the account data
            switch (userProfileType)
            {
                case UserProfileType.ServiceUser:
                    // get all the service accounts
                    serviceAccounts = _session.QueryOver<ServiceAccount>().List();
                    break;
                case UserProfileType.VendorUser:
                    // get the parent and child service accounts based of the user 
                    serviceAccounts = GetlinkedServiceAccounts(userId);
                    break;
               
            }
            return serviceAccounts;
        }

        private IEnumerable<ServiceAccount> GetlinkedServiceAccounts(int userId)
        {
            //CriteriaTransformer.
            // _session.QueryOver<UserServiceAccount>().Where(i=>i.RoleId)

            // apply role base logic to filter accounts
            //TODO: fetch all user mapped service accounts and their child service accounts
            return
                _session.QueryOver<UserServiceAccount>()
                        .Where(i => i.UserId.Equals(userId)).List().Select(i => i.ServiceAccounts);
        }

        public ServiceAccount GetServiceAccountById(int serviceAccountId)
        {
            return _session.Get<ServiceAccount>(serviceAccountId);
        }

        public void UpdateServiceAccount(ServiceAccount serviceAccount)
        {
            using (var transaction = _session.BeginTransaction())
            {
              

                _session.Update(serviceAccount);
                transaction.Commit();
            }
        }

        public void AddNewServiceAccount(ServiceAccount serviceAccount)
        {
            using (var transaction = _session.BeginTransaction())
            {
                var result = _session.Save(serviceAccount);
                transaction.Commit();
            }
             
        }

        public void DeleteServiceAccountById(int serviceAccountId)
        {
            //using (var transaction = _session.BeginTransaction())
            //{
            //    var serviceAccount = _session.Get<ServiceAccount>(serviceAccountId);
            //    _session.Delete(serviceAccount);
            //    transaction.Commit();
            //}
            
        }

       
    }
}
