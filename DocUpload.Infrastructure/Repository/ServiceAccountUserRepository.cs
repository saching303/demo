﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using DocUpload.Core;
using DocUpload.Core.Interface;
using DocUpload.Core.Model;
using NHibernate;
using NHibernate.Criterion;

namespace DocUpload.Infrastructure.Repository
{
    public class ServiceAccountUserRepository : IServiceAccountUserRepository
    {
         readonly ISession _session;

        public ServiceAccountUserRepository(ISession session)
        {
            _session = session;
        }
        public IEnumerable<UserServiceAccount> GetMappedServiceAccoutsByUser(User user)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserProfile> GeServiceAccoutUsersByOwner(User user)
        {
            throw new NotImplementedException();
        }

        public UserProfile GetServiceAccountUserById(int userId)
        {
            throw new NotImplementedException();
        }

        public void AddNewServiceAccountUser(UserProfile user, IEnumerable<UserServiceAccount> userServiceAccount)
        {
            throw new NotImplementedException();
        }

        public void UpdateServiceAccountUser(UserProfile user, IEnumerable<UserServiceAccount> userServiceAccount)
        {
            throw new NotImplementedException();
        }

        public void DeleteServiceAccountUserById(int userId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserType> GetUserTypes()
        {
            throw new NotImplementedException();
        }

        public UserType GetUserType(int userType)
        {
            return GetUserTypes().FirstOrDefault(x => x.Id == userType);
        }


        public IEnumerable<UserServiceAccount> GetServiceAccountUsersByAccountIds(int[] accountIds)
        {
            return
                _session.QueryOver<UserServiceAccount>()
                        .WhereRestrictionOn(x => x.ServiceAccountId)
                        .IsIn(accountIds)
                        .List();
        }
    }
}
