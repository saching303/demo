Insert into NDTS.SERVICEACCOUNT
   (ID, NAME, EMAIL, ACTIVE, CREATEDDATE, 
    UPDATEDDATE, CREATEDBY, UPDATEDBY, CONNSTRING, PARENTSERVICEACCOUNTID)
 Values
   (SERVICEACCOUNT_SEQ.Nextval, 'Nunit Test Account', 'nunittest@gmail.com ', 'N', sysdate, 
    null, 999999, 2, 'Nunit Test Connection String', 0);


Insert into NDTS.RECIPIENTSETTING
   (ID, PASSWORD, USERNAME, SERVICEACCOUNTID, RECEIPIENTID)
 Values
   (RECIPIENTSETTING_SEQ.Nextval, 'testing password', 'testing user', 104, 1);
