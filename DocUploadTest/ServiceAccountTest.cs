﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using DocUpload.Core.Interface;
using DocUpload.Core.Model;
using DocUpload.Infrastructure;
using NHibernate;
using NHibernate.Linq;
using NUnit.Framework;

namespace DocUploadTest
{
    [TestFixture]
    public class ServiceAccountTest
    {
        private IServiceAccountRepository _iServiceAccountRepository;
        private ILifetimeScope _iLifetimeScope;
        private ISession _iSession;
        private int _serviceAccountId;
        private int _recipientId;
        private int _userId;
        //private ServiceAccount _serviceAccountWithoutRecipient;
        //private ServiceAccount _serviceAccountWithRecipient;

        [SetUp]
        public void Init()
        {
            _serviceAccountId = 78;
            _userId = 999999;
            _recipientId = 1;
            var builder = new ContainerBuilder();
            builder.Register(
                context => FluentNHibernateHelper.CreateNewSession())
                   .As<ISession>().InstancePerLifetimeScope();

            builder.RegisterModule<DocUploadTypesModule>();
            var container = builder.Build();
            _iLifetimeScope = container.BeginLifetimeScope();
            _iServiceAccountRepository = _iLifetimeScope.Resolve<IServiceAccountRepository>();
            _iSession = _iLifetimeScope.Resolve<ISession>();
        }

        [TearDown]
        public void Dispose()
        {
            _iLifetimeScope.Dispose();
            _iSession.Dispose();
        }

        [TestCase]
        public void GetServiceAccountTest()
        {
            var serviceAccount = _iServiceAccountRepository.GetServiceAccountById(_serviceAccountId);

            Assert.AreEqual(serviceAccount.Id,_serviceAccountId);
        }

        [TestCase]
        public void AddServiceAccountWithOutRecipientTest()
        {
            var serviceAccount = new ServiceAccount
                {
                    Name = "AddServiceAccountWithOutRecipientTest ServiceAccount " + DateTime.Now,
                    ConnString = "Testing Connection",
                    Active = 'N',
                    CreatedBy = _userId,
                    CreatedDate = DateTime.Now
                };

            _iServiceAccountRepository.AddNewServiceAccount(serviceAccount);

            var newServiceAccount = _iSession.Query<ServiceAccount>()
                                          .Where(s => s.Name.Equals(serviceAccount.Name))
                                          .ToList()
                                          .FirstOrDefault();
            
            Assert.AreEqual(newServiceAccount.Name, serviceAccount.Name);
            Assert.AreEqual(newServiceAccount.CreatedBy, _userId);
        }

        [TestCase]
        public void AddServiceAccountWithRecipientTest()
        {
            var recipientobj = _iSession.Get<Recipient>(_recipientId);

            var serviceAccount = new ServiceAccount
            {
                Name = "AddServiceAccountWithRecipientTest ServiceAccount " + DateTime.Now,
                ConnString = "Testing Connection",
                Active = 'N',
                CreatedBy = _userId,
                CreatedDate = DateTime.Now
            };

            var serviceAccountRecipient = new ServiceAccountRecipient
                {
                    Recipient = recipientobj,
                    Password = "AddServiceAccountWithRecipientTest Recipient " + DateTime.Now,
                    UserName = "AddServiceAccountWithRecipientTest Password " + DateTime.Now,
                    RecipientId = recipientobj.Id,
                    ServiceAccount = serviceAccount
                };

            var recipients = new List<ServiceAccountRecipient> { serviceAccountRecipient };

            serviceAccount.Recipients = recipients;

            _iServiceAccountRepository.AddNewServiceAccount(serviceAccount);

            var newServiceAccount = _iSession.Query<ServiceAccount>()
                                          .Where(s => s.Name.Equals(serviceAccount.Name))
                                          .ToList()
                                          .FirstOrDefault();

            var addedRecipient = newServiceAccount.Recipients.FirstOrDefault();

            Assert.AreEqual(newServiceAccount.Name, serviceAccount.Name);
            Assert.AreEqual(newServiceAccount.Email, serviceAccount.Email);
            Assert.AreEqual(addedRecipient.UserName, serviceAccountRecipient.UserName);
            Assert.AreEqual(addedRecipient.Password, serviceAccountRecipient.Password);

        }

        [TestCase]
        public void UpdateServiceAccountWithoutRecipientTest()
        {
            var serviceAccount =CreateServiceAccountWithoutRecipient();

            serviceAccount.Name = "UpdateServiceAccountWithoutRecipientTest ServiceAccount " + DateTime.Now;
            serviceAccount.Email = "updatednunittest@gmail.com";
            serviceAccount.UpdatedDate = DateTime.Now;
            serviceAccount.UpdatedBy = _userId;

            _iServiceAccountRepository.UpdateServiceAccount(serviceAccount);

            var updatedServiceAccount = _iSession.Get<ServiceAccount>(serviceAccount.Id);

            Assert.AreEqual(updatedServiceAccount.Name, serviceAccount.Name);
            Assert.AreEqual(updatedServiceAccount.Email, serviceAccount.Email);
        }

        [TestCase]
        public void UpdateServiceAccountWithRecipientTest()
        {
            var serviceAccount = CreateServiceAccountWithRecipient();

            serviceAccount.Name = "UpdateServiceAccountWithRecipientTest ServiceAccount" + DateTime.Now;
            serviceAccount.Email = "updatednunittest@gmail.com";
            serviceAccount.UpdatedDate = DateTime.Now;
            serviceAccount.UpdatedBy = _userId;

            var recipient = serviceAccount.Recipients.FirstOrDefault();

            recipient.UserName = "UpdateServiceAccountWithRecipientTest Recipient username" + DateTime.Now;
            recipient.Password = "UpdateServiceAccountWithRecipientTest Recipient password" + DateTime.Now;

            _iServiceAccountRepository.UpdateServiceAccount(serviceAccount);

            var updatedServiceAccount = _iSession.Get<ServiceAccount>(serviceAccount.Id);
            var updatedRecipient = updatedServiceAccount.Recipients.FirstOrDefault();

            Assert.AreEqual(updatedServiceAccount.Name, serviceAccount.Name);
            Assert.AreEqual(updatedServiceAccount.Email, serviceAccount.Email);
            Assert.AreEqual(updatedRecipient.UserName, recipient.UserName);
            Assert.AreEqual(updatedRecipient.Password, recipient.Password);
        }

        [TestCase]
        public void AddFirstRecipientToServiceAccountTest()
        {
            var serviceAccount = CreateServiceAccountWithoutRecipient();
            var recipientobj = _iSession.Get<Recipient>(_recipientId);

            var serviceAccountRecipient = new ServiceAccountRecipient
            {
                Recipient = recipientobj,
                Password = "AddFirstRecipientToServiceAccountTest Recipient Password" + DateTime.Now,
                UserName = "AddFirstRecipientToServiceAccountTest Recipient username" + DateTime.Now,
                RecipientId = recipientobj.Id,
                ServiceAccount = serviceAccount
            };

            var sarList = new List<ServiceAccountRecipient>
                {
                    serviceAccountRecipient
                };

            serviceAccount.Recipients = sarList;

            _iServiceAccountRepository.UpdateServiceAccount(serviceAccount);


            var updatedServiceAccount = _iSession.Get<ServiceAccount>(serviceAccount.Id);
            var addedRecipient = updatedServiceAccount.Recipients.FirstOrDefault();

            Assert.AreEqual(addedRecipient.UserName, serviceAccountRecipient.UserName);
            Assert.AreEqual(addedRecipient.Password, serviceAccountRecipient.Password);
        }

        //[TestCase]
        //public void AddSecondRecipientToServiceAccountTest()
        //{
        //    var serviceAccount = CreateServiceAccountWithRecipient();
        //    var recipientobj = _iSession.Get<Recipient>(_recipientId);

        //    var serviceAccountSecondRecipient = new ServiceAccountRecipient
        //    {
        //        Recipient = recipientobj,
        //        Password = "AddSecondRecipientToServiceAccountTest Recipient Password" + DateTime.Now,
        //        UserName = "AddSecondRecipientToServiceAccountTest Recipient username" + DateTime.Now,
        //        RecipientId = recipientobj.Id,
        //        ServiceAccount = serviceAccount
        //    };

        //    var sarList = new List<ServiceAccountRecipient>
        //        {
        //            serviceAccountSecondRecipient
        //        };

        //    serviceAccount.Recipients = sarList;

        //    _iServiceAccountRepository.UpdateServiceAccount(serviceAccount);

        //    _iSession.Clear();

        //    using (var transaction = _iSession.BeginTransaction())
        //    {
        //        var updatedServiceAccount = _iSession.Get<ServiceAccount>(serviceAccount.Id);
        //        var updatedRecipient = updatedServiceAccount.Recipients.FirstOrDefault();

        //        Assert.AreEqual(updatedServiceAccount.Recipients.Count(), 2);
        //        Assert.AreEqual(updatedRecipient.UserName, serviceAccountSecondRecipient.UserName);
        //        Assert.AreEqual(updatedRecipient.Password, serviceAccountSecondRecipient.Password);

        //        transaction.Commit();
        //    }
            
        //}

        //[TestCase]
        //public void UpdateExistingRecipientAndAddNewRecipientToServiceAccountTest()
        //{
        //    var serviceAccount = CreateServiceAccountWithRecipient();
        //    var recipientobj = _iSession.Get<Recipient>(_recipientId);

        //    var serviceAccountSecondRecipient = new ServiceAccountRecipient
        //    {
        //        Recipient = recipientobj,
        //        Password = "UpdateExistingRecipientAndAddNewRecipientToServiceAccountTest Recipient Password",
        //        UserName = "UpdateExistingRecipientAndAddNewRecipientToServiceAccountTest Recipient username",
        //        RecipientId = recipientobj.Id,
        //        ServiceAccount = serviceAccount
        //    };

        //    var sarList = new List<ServiceAccountRecipient>
        //        {
        //            serviceAccountSecondRecipient
        //        };

        //    serviceAccount.Recipients = sarList;

        //    _iServiceAccountRepository.UpdateServiceAccount(serviceAccount);


        //    var updatedServiceAccount = _iSession.Get<ServiceAccount>(serviceAccount.Id);
        //    var updatedRecipient = updatedServiceAccount.Recipients.Skip(1).FirstOrDefault();

        //    Assert.AreEqual(updatedRecipient.UserName, serviceAccountSecondRecipient.UserName);
        //    Assert.AreEqual(updatedRecipient.Password, serviceAccountSecondRecipient.Password);
        //}

        //[TestCase]
        //public void UpdateRecipientToServiceAccountTest()
        //{

        //}

        private ServiceAccount CreateServiceAccountWithoutRecipient()
        {
            var serviceAccount = _iSession.Query<ServiceAccount>()
                                          .Where(s => s.Name.Equals("Nunit Test Account Without Recipient"))
                                          .ToList()
                                          .FirstOrDefault();
            return serviceAccount;
        }

        private ServiceAccount CreateServiceAccountWithRecipient()
        {
            var serviceAccount = _iSession.Query<ServiceAccount>()
                                          .Where(s => s.Name.Equals("Nunit Test Account With Recipient"))
                                          .ToList()
                                          .FirstOrDefault();
            return serviceAccount;
        }
    }
}
