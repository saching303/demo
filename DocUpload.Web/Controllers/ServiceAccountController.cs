﻿using System.Web.Mvc;
using DocUpload.Core.Interface;
using DocUpload.Core.Model;
using System.Collections.Generic;

namespace DocUpload.Web.Controllers
{
    public class ServiceAccountController : Controller
    {
        private readonly IServiceAccountRepository _serviceAccountRepository;

        public ServiceAccountController(IServiceAccountRepository serviceAccountRepository)
        {
            _serviceAccountRepository = serviceAccountRepository;
        }

        //
        // GET: /ServiceAccount/

        public ActionResult Index()
        {
            var serviceAccount = new ServiceAccount();
            return View(serviceAccount);
        }

        public JsonResult AddServiceAccount(ServiceAccount serviceAccount)
        {
            //var recipients = new List<ServiceAccountRecipient>
            //    {
            //        new ServiceAccountRecipient {Password = "abc", UserName = "BDFTexas", RecipientId = 1, ServiceAccount = serviceAccount},
            //        new ServiceAccountRecipient {Password = "abcd", UserName = "BDFTexas", RecipientId = 2, ServiceAccount =  serviceAccount}
            //    };
            serviceAccount.CreatedBy = 1;
            serviceAccount.CreatedDate = System.DateTime.Now;
            serviceAccount.ConnString = "ConnString";
            //serviceAccount.Recipients = recipients;
            _serviceAccountRepository.AddNewServiceAccount(serviceAccount);
            return Json(new{msg="Account created successfully!"});
        }

    }
}
