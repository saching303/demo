﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DocUpload.Core.Interface;
using DocUpload.Core.Model;

namespace DocUpload.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IServiceAccountRepository _serviceAccountRepository;
        private readonly IServiceAccountUserRepository _serviceAccountUserRepository;

        public HomeController(IServiceAccountRepository serviceAccountRepository, IServiceAccountUserRepository serviceAccountUserRepository)
        {
            _serviceAccountRepository = serviceAccountRepository;
            _serviceAccountUserRepository = serviceAccountUserRepository;
        }

        public ActionResult Index()
        {
          var serviceAccounts = _serviceAccountRepository.GetServiceAccountsByUser(1,UserProfileType.ServiceUser);
           var serviceAccountsUsers= _serviceAccountUserRepository.GetServiceAccountUsersByAccountIds(serviceAccounts.Select(i => i.Id).ToArray());
            return View();
        }

        
        public ActionResult Create()
        {
            var serviceAccount = new ServiceAccount
            {
                ParentServiceAccountId = 1,
                CreatedBy = 1,
                CreatedDate = System.DateTime.Now,
                Email = "g@gmail.com",
                Name = "1BDF Texas",
                UpdatedBy = 1,
                UpdatedDate = System.DateTime.Now,
                IsActive = true,
                ConnString = "newString"
            };

            var recipients = new List<ServiceAccountRecipient>
                {
                    new ServiceAccountRecipient {Password = "abc", UserName = "BDFTexas", RecipientId = 1, ServiceAccount = serviceAccount},
                    new ServiceAccountRecipient {Password = "abcd", UserName = "BDFTexas", RecipientId = 2, ServiceAccount =  serviceAccount}
                };

            serviceAccount.Recipients = recipients;
            
            _serviceAccountRepository.AddNewServiceAccount(serviceAccount);
            return View("~/Views/Home/Index.cshtml");
        }

    }
}
